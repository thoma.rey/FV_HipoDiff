# Discrete Hypocoercivity and Diffusion Limit for Linear Kinetic equations

Python implementation of the numerical method presented in the paper [BessemoulinHerdaRey2018], with many different numerical test cases.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/thoma.rey%2FFV_HipoDiff/master?filepath=kindiff_lib.ipynb)