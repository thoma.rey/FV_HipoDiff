import numpy as np
import matplotlib.pyplot as plt


#####################################
# DISCRETIZATION OF THE PHASE SPACE #
#####################################

def discretizeV(L,vstar):
    # Brief: Creates a discretization of the symmetric velocity domain [-vstar, vstar] with 2L cells.
    # Input: 
    # - vstar: right endpoint
    # - L: Number of cells with positive velocities
    # Output: 
    # - v: Cell centers
    # - vhalf: Cell interfaces
    # - dv: Cell length
    # - Nv: Number of cells
    # - vmin: left endpoint
    # - vmax: right endpoint
    vmax = vstar
    vmin = -vstar
    Nv = 2 * L
    vhalf = np.linspace(vmin,vmax, Nv+1) # Cell interface points
    dv = np.diff(vhalf) # Cell length
    v = vhalf[:len(vhalf)-1] + dv*.5 # Cell centers
    return (v,vhalf,dv,Nv,vmin,vmax)

def discretizeX(Nx,X):
    # Brief: Creates a discretization of the periodic space domain of length X with Nx cells. Warning: if Nx is even it is changed to Nx+1.
    # Input: 
    # - X: Length of the domain
    # - Nx: Number of cells
    # Output: 
    # - x: Cell centers
    # - xhalf: Cell interfaces
    # - dx: Cell length
    # - Nx: Number of cells
    # - xmin: left endpoint
    # - xmax: right endpoint
    
    if Nx % 2 == 0:
        Nx = Nx + 1

    xmin = 0
    xmax = X

    xhalf = np.linspace(xmin,xmax, Nx + 1) # Cell interface points
    dx = np.diff(xhalf) # Cell length
    xhalf = xhalf[:len(xhalf)-1] # Interface points
    x = xhalf + dx*0.5  # Cell centers
    return (x,xhalf,dx,Nx,xmin,xmax)

def discretizeXV(x,Nx,dx,v,Nv,dv):
    # Brief: Puts the discretization of the phase space in meshgrid (matrix) form. Rows are velocities and columns are positions.
    #
    # Input: 
    # - x: Cell centers in position
    # - Nx: Number of cells in position
    # - dx: Length of cells in position
    # - v: Cell centers in position
    # - Nv: Number of cells in velocities
    # - dv: Length of cells in velocities
    #
    # Output: 
    # - X: Cell centers in position (meshgrid form)
    # - DX: Length of cells in position (meshgrid form)
    # - V: Cell centers in position (meshgrid form)
    # - DV: Length of cells in velocities (meshgrid form)
    # - N: Number of cells in phase space
    [X,V] = np.meshgrid(x,v)
    DV = np.transpose(np.tile(dv,(Nx,1)))
    DX = np.tile(dx,(Nv,1))
    N = Nx * Nv
    return (X,V,DV,DX,N)

#####################################
# DISCRETE MAXWELLIAN               #
#####################################
def gaussian(u):
    # Brief: Gaussian distribution
    return np.exp(-u * u / 2) / np.sqrt(2 * np.pi)

def heavy(u):
    # Brief: A weird heavy-tailed distribution
    return 1/(1+.1*np.abs(u)**4)*(np.cos(np.pi*u)+1.1)


def Maxwellian(equation, distrib,v, vhalf,dv,Nx):
    # Brief: Compute the Maxwellian distribution from the velocity discretization
    #
    # Input:
    # - equation: 1(Fokker-Planck), 2(BGK), 3(Heavy-tailed BGK)
    # - distrib: the distribution function
    # - v: cell centers 
    # - vhalf: interface points
    # - dv: cell length
    # - Nx: number of cells in x 
    #
    # Output:
    # - M: Maxwellian at cell center 
    # (- Mhalf: Maxwellian at interfaces)
    # - MM: Maxwellian at cell center in meshgrid form
    if equation == 1: # Fokker-Planck
        Mhalf = distrib(vhalf) # At interfaces
        Mhalf[len(Mhalf)-1] = 0 
        Mhalf[0] = 0
        M = - np.diff(Mhalf) / v / dv # In cells
        MassM = np.sum(M * dv) 
        M = M / MassM # Normalize mass
        Mhalf = Mhalf / MassM
        MM = np.transpose(np.tile(M,(Nx,1)))
    else: # BGK
        M = distrib(v) # In cells
        MassM = np.sum(M * dv)
        M = M / MassM # Normalize mass
        MM = np.transpose(np.tile(M,(Nx,1)))
        Mhalf = [] # Interfaces are not needed
    return (M,Mhalf,MM)



#####################################
# INITIAL DATA                      #
#####################################   

def init_Oscillation(X,V,xstar):
    # Brief: Perturbed Gaussian
    F = gaussian(V)* (1. + 1.*np.cos(2 * np.pi * X / xstar))
    return F

def init_FullRandom(X,V): 
    # Brief: random distribution
    F = np.random.random_sample(np.shape(V))
    return F

def init_TruncatedRandom(X,V):
    # Brief: random distribution supported on low velocities
    F = np.random.random_sample(np.shape(V))*(abs(V)< 3)
    return F

def init_ball(X,V,xstar,vstar):
    # Brief: indicator function of an ellipse
    F = 1.*((X-0.5*xstar)**2+(V/2/vstar)**2 < 0.2**2)
    return F 

def init_other(X,V,xstar):
    # Brief: Non-uniform in x, non-gaussian in v distribution
    F =  gaussian(V)*V**4*(1+1*np.cos(2 * np.pi * X / xstar))
    return F

def SteadyState(F,MM,DX,DV,xstar):
    # Brief: Computes the steady state
    #
    # Input:
    # - F: transient state in meshgrid form
    # - MM: Maxwelllian in meshgrid form
    # - DX,DV: Cell length in meshgrid form
    # - xstar: length of the space dommain
    #
    # Output:
    # - mf: Mass
    # - Finf: Steady state in meshgrid form
    mf = np.sum(F * DX * DV) / xstar
    Finf = MM * mf
    return (Finf,mf)

def initAll(F, Finf, MM, DV, mf, epsilon):
    # Brief: Computes all useful macroscopic and microscopic quantities from F and Finf
    #
    # Input: 
    # - F: transient state in meshgrid form
    # - Finf: Steady state in meshgrid form
    # - MM: Maxwelllian in meshgrid form
    # - DV: Cell length in meshgrid form
    # - mf: Mass
    # - epsilon: Knudsen number
    #
    # Output: 
    # - f: transient state
    # - rho: macroscopic density
    # - Rho: macroscopic density in meshgrid form
    # - h: microscopic unknown
    # - H: microscopic unknown in meshgrid form 
    # - lamb: macroscopic unknown
    # - Lamb: macroscopic unknown in meshgrid form 
    # - finf: Steady state
    # - rhoinf: macroscopic steady state
    N = F.size
    Nv = F.shape[0]
    Nx = F.shape[1]
    f = np.reshape(F,N)
    rho = np.transpose(np.sum(F * DV,0))
    Rho = np.tile(rho,(Nv,1))
    finf = np.reshape(Finf,(N,1))
    rhoinf = 0*rho.copy() + mf
    if (epsilon>0):
        H = (F / MM  - Rho) / epsilon
    else:
        H = 0 * F
    h = np.reshape(H,N)
    lamb = np.reshape(rho - mf,Nx)
    Lamb = Rho - mf 
    return (f,rho,Rho,h,H,lamb,Lamb,finf,rhoinf)

def computeDens(unknown, MM, DV, mf, epsilon):
    # Brief: Computes all useful macroscopic and microscopic quantities from the vector of unknown
    #
    # Input: 
    # - F: transient state in meshgrid form
    # - Finf: Steady state in meshgrid form
    # - MM: Maxwelllian in meshgrid form
    # - DV: Cell length in meshgrid form
    # - mf: Mass
    # - epsilon: Knudsen number
    #
    # Output: 
    # - f: transient state
    # - F: transient state in meshgrid form
    # - rho: macroscopic density
    # - Rho: macroscopic density in meshgrid form
    # - h: microscopic unknown
    # - H: microscopic unknown in meshgrid form 
    # - lamb: macroscopic unknown
    # - Lamb: macroscopic unknown in meshgrid form 
    N = MM.size
    Nv = MM.shape[0]
    Nx = MM.shape[1]
    
    
    lamb = unknown[0:Nx]
    Lamb = np.tile(lamb,(Nv,1))
    h = unknown[Nx:]
    H = np.reshape(h, (Nv,Nx)) 
    F = (mf + Lamb + epsilon * H) * MM
    f = np.reshape(F,N)
    rho = np.transpose(np.sum(F * DV,0))
    Rho = np.tile(rho,(Nv,1))
    return (f,F,rho,Rho,h,H,lamb,Lamb)

#####################################
# ASSEMBLING MATRICES               #
#####################################   

def indx(i,Nx):
    # Brief: matrix index of macroscopic unknown
    return i % Nx
def ind(i,j,Nx):
    # Brief: matrix index of microscopic unknown
    return indx(i,Nx) + j * Nx

def kinMat(equation,M,Mhalf,v,dv,dx,dt,epsilon):
    # Brief: Assembling matrix to be inverted to solve kinetic equation
    Nx = len(dx)
    Nv = len(dv)
    N = Nx*Nv
    D_ll = np.zeros((Nx,Nx))
    D_lh = np.zeros((Nx, N))
    D_hl = np.zeros((N, Nx))
    D_hh = np.zeros((N,N))

    Mat_ll = np.zeros((Nx,Nx))       
    Mat_lh = np.zeros((Nx, N))
    Mat_hl = np.zeros((N, Nx))
    Mat_hh = np.zeros((N,N))
    Mat_N = np.zeros((Nx, N))

    for i in range(0,Nx):
        for j in range(0,Nv):

            # Time derivative parts

            D_ll[indx(i,Nx), indx(i,Nx)] = 1.
            D_hh[ind(i,j,Nx), ind(i,j,Nx)] = epsilon * epsilon
            Mat_ll[indx(i,Nx), indx(i,Nx)] = 1.
            Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] = epsilon * epsilon

            # Transport in the lambda equation 
            Mat_lh[indx(i,Nx),ind(i+1,j,Nx)] = dt * v[j] * M[j] * dv[j] /  dx[i] * .5
            Mat_lh[indx(i,Nx),ind(i-1,j,Nx)] = - dt * v[j] * M[j] * dv[j] /  dx[i] * .5


            # Transport in lambda in the h equation 
            Mat_hl[ind(i,j,Nx), indx(i+1,Nx)] = dt * v[j] /  dx[i] * .5
            Mat_hl[ind(i,j,Nx), indx(i-1,Nx)] = - dt * v[j] /  dx[i] * .5

            # Transport in h in the h equation 
            Mat_hh[ind(i,j,Nx), ind(i+1,j,Nx)] = epsilon * dt * v[j] * (1 - M[j] * dv[j]) /  dx[i] * .5
            Mat_hh[ind(i,j,Nx), ind(i-1,j,Nx)] = - epsilon * dt * v[j] * (1 - M[j] * dv[j]) /  dx[i] * .5
            for k in range(0,Nv):
                if k!=j:
                    Mat_hh[ind(i,j,Nx), ind(i+1,k,Nx)] = - epsilon * dt * v[k] * M[k] * dv[k] /  dx[i] * .5
                    Mat_hh[ind(i,j,Nx), ind(i-1,k,Nx)] = + epsilon * dt * v[k] * M[k] * dv[k] /  dx[i] * .5

            # Collision part in the h equation

            if equation == 1: # Kinetic FP
                if j != Nv-1:
                    Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] = Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] + dt * Mhalf[j+1] / M[j] / (v[j+1] - v[j]) / dv[j]
                    Mat_hh[ind(i,j,Nx), ind(i,j+1,Nx)] = - dt * Mhalf[j+1] / M[j] / (v[j+1] - v[j]) / dv[j]
                if j != 0:
                    Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] = Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] + dt * Mhalf[j] / M[j] / (v[j] - v[j-1]) / dv[j]
                    Mat_hh[ind(i,j,Nx), ind(i,j-1,Nx)] = - dt * Mhalf[j] / M[j] / (v[j] - v[j-1]) / dv[j]
            else: # BGK
                Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] = Mat_hh[ind(i,j,Nx), ind(i,j,Nx)] + dt

            # Mean free part in h

            Mat_N[indx(i,Nx), ind(i,j,Nx)] = M[j] * dv[j]

    Mat = np.block([[Mat_ll, Mat_lh],
                    [Mat_hl, Mat_hh],
                    [np.zeros((Nx, Nx)), Mat_N],
                    [dx.reshape(1,Nx), np.zeros((1,N))]])  

    D = np.block([[D_ll, D_lh],
                  [D_hl, D_hh],
                  [np.zeros((Nx+1,Nx)), np.zeros((Nx+1,N))]])
    return Mat, D

def heatMat(M,v,dv,dx,dt):
    # Brief: Assembling matrix to be inverted to solve macroscopic equation
    Nx = len(dx)
    m2dv = np.sum(M * v * v * dv)

    Mat_heat = np.eye(Nx)

    for i in range(0,Nx):
        Mat_heat[indx(i,Nx), indx(i+2,Nx)] = - m2dv * dt / (2. * dx[indx(i+1,Nx)]) / (2. * dx[indx(i,Nx)]) 
        Mat_heat[indx(i,Nx), indx(i,Nx)] = Mat_heat[indx(i,Nx), indx(i,Nx)] +  m2dv * dt *(1 / (2. * dx[indx(i+1,Nx)]) / (2. * dx[indx(i,Nx)]) + 1 / (2. * dx[indx(i-1,Nx)]) / (2. * dx[indx(i,Nx)])) 
        Mat_heat[indx(i,Nx), indx(i-2,Nx)] = - m2dv * dt / (2. * dx[indx(i-1,Nx)]) / (2. * dx[indx(i,Nx)])
    
    Mat_heat = np.block([[Mat_heat],
                         [dx.reshape((1,Nx))]])
    D_heat = np.block([[np.eye(Nx)],
                       [np.zeros((1,Nx))]])
    return (Mat_heat, D_heat)


def invMat(Mat):
    # Brief: Compute the inverse of a rectangular full-rank matrix using SVD
    #
    # Input:
    # - Mat: matrix to be inverted
    #
    # Output:
    #
    # -Matinv: (pseudo-)inverse matrix
    siz = np.shape(Mat)[1]
    [U,Sigma,Vh] = np.linalg.svd(Mat)
    Ut  = U.T
    Vht = Vh.T
    Matinv = Vht[:,:siz]@np.diag(1./Sigma)@Ut[:siz,:]
    return Matinv


def diagnostic(F, H, lamb, lamb_heat, MM, DV, DX, epsilon):
    # Brief: Compute control variates
    L1 = np.sum(F * DV * DX)
    L2h = np.sum(H * H * MM * DV * DX)
    L2lamb = np.sum(lamb * lamb * DX[0,:])
    L2f = epsilon * epsilon * L2h + L2lamb
    L2lamb_heat = np.sum(lamb_heat * lamb_heat * DX[0,:])
    return (L1, np.sqrt(L2h), np.sqrt(L2lamb), np.sqrt(L2f), np.sqrt(L2lamb_heat))